/**
 * frisby.js: quarkcloud API Testing
 * 9/12/2014 Isabel Lu
 */ 

var frisby = require('/usr/local/lib/node_modules/frisby');

//Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
}); 
   
var access_token="";


frisby.create('Get project schema should return success')
	.get('http://api.quarkcloudtech.com:1337/api/schema/project')
	.expectStatus(200)
    .inspectJSON() 
 //    .expectJSONTypes({
	//     project:{
	// 	  	name:{
	// 	  		'type':String,
	// 	  		'required':Boolean
	// 	 	},
	// 	 	desc:{
	//   			'type':String
	//   		},
	//   	    tags:{
	// 	  		'type':String,
	// 	 		'defaultsTo':Array,
	// 	 	},
	//   		crs:{
	// 	  		'type':String,
	// 	  		'enum':Array,
	// 	  		'defaultsTo':String	  			
	// 	  	},
	// 	  	measurementUnit:{
	// 	  		'length':{
	// 	  				'type':String,
	// 	  				'enum':Array,
	// 	 				'defaultsTo':String
	// 	 			},
	// 	 		'time':{
	// 	 			'type':String,
	// 	 			'enum':Array,
	//   				'defaultsTo':String
	//  			},
	//   			'pumpimgRate':{
	// 	  			'type':String,
	// 	  			'enum':Array,
	// 	  			'defaultsTo':String
	// 	 		},
	// 	 		'rechargeRate':{
	// 	 			'type':String,
	// 	  			'enum':Array,
	// 	  			'defaultsTo':String
	// 	 		},
	// 	 		'specificStorage':{
	// 	  			'type':String,
	// 	 			'enum':Array,
	// 	  			'defaultsTo':String
	// 	 		},
	// 	 		'project':{
	// 	 			'model':String
	//   			},
	//   			objType:{
	// 	 			'type':String,
	// 				'enum':Array,
	// 	 			'defaultsTo':String
	// 	 		},
	// 		 	objFormat:{
	// 		 		'type':String,
	// 		  		'enum':Array,
	// 		  		'defaultsTo':String
	// 		  	},
	// 		  	objSource:{
	// 		  		'type':String,
	// 		  		'enum':Array,
	// 		  		'defaultsTo':String
	// 		  	},
	// 		  	schemaVersion:{
	// 		  		'type':String
	// 		  	},
	// 		  	id:{
	// 		  		'type':String,
	// 		  		autoIncrement:Boolean,
	// 		  		primaryKey:Boolean,
	// 		  		unique:Boolean
	// 		  	},
	// 		  	createdAt:{
	// 		  		type:String,
	// 		  		default:String
	// 		  	},
	// 		  	updatedAt:{
	// 		  		type:String,
	// 		  		default:String
	// 		  	}
	// 	 	},
	// 	 		datasets:{
	// 	 			'collection':String,
	// 		  		'via':String,
	// 		  		'defaultsTo':Array
	// 	 		},
	// 	 		models:{
	// 	  			'collection':String,
	// 		  		'via':String,
	// 	 			'defaultsTo':Array
	// 	 		},
	//   		owner:{
	// 	  		'type':String,
	// 	  		'required':Boolean
	// 	 		},
	// 	    objType:{
	// 	 	    'type':String,
	// 			'enum':Array,
	// 	 		'defaultsTo':String
	// 	 		},
	// 	 	objFormat:{
	// 	 		'type':String,
	// 	  		'enum':Array,
	// 	  		'defaultsTo':String
	// 	  	},
	// 	  	objSource:{
	// 	  		'type':String,
	// 	  		'enum':Array,
	// 	  		'defaultsTo':String
	// 	  	},
	// 	  	schemaVersion:{
	// 	  		'type':String,
	// 	  		'defaultsTo':undefined 
	// 	  	},
	// 	  	id:{
	// 	  		'type':String,
	// 	  		autoIncrement:Boolean,
	// 	  		primaryKey:Boolean,
	// 	  		unique:Boolean
	// 	  	},
	// 	  	createdAt:{
	// 	  		type:String,
	// 	  		default:String
	// 	  	},
	// 	  	updatedAt:{
	// 	  		type:String,
	// 	  		default:String
	// 	  	}
	//     }
	// })
   //  .expectJSON({
	  // 	"project": {
		 //    "name": {
		 //        "type": "string",
		 //        "required": true
		 //    },
		 //    "desc": {
		 //        "type": "string"
		 //    },
		 //    "tags": {
		 //        "type": "json",
		 //        "defaultsTo": []
		 //    },
		 //    "crs": {
		 //        "type": "string",
		 //        "enum": [
	  //               "WGS84",
		 //            "NAD 1927"
		 //            ],
		 //        "defaultsTo": "WGS84"
		 //    },
		 //    "measurementUnit": {
		 //        "length": {
		 //            "type": "string",
		 //            "enum": [
		 //                "km",
			//             "m",
			//             "cm",
			//             "mm",
			//             "in",
			//             "ft",
			//             "mi"
			//         ],
		 //            "defaultsTo": "m"
		 //        },
		 //        "time": {
		 //            "type": "string",
		 //            "enum": [
	  //                   "year",
		 //                "month",
		 //                "day",
		 //                "hour",
		 //                "minute",
		 //                "second"
		 //            ],
		 //            "defaultsTo": "year"
		 //        },
		 //        "pumpimgRate": {
		 //            "type": "string",
		 //            "enum": [
	  //                   "m3/s",
		 //                "ft3/s"
		 //            ],
		 //            "defaultsTo": "m3/s"
		 //        },
		 //        "rechargeRate": {
		 //            "type": "string",
		 //            "enum": [
		 //            	"km/s",
			//             "km/hr",
			//             "m/s",
			//             "m/hr",
			//             "cm/s",
			//             "cm/hr",
			//             "mm/s",
			//             "mm/hr",
			//             "in/s",
			//             "in/hr",
			//             "ft/s",
			//             "ft/hr",
			//             "mi/s",
			//             "mi/hr"
			//         ],
		 //            "defaultsTo": "m/s"
		 //        },
		 //        "specificStorage": {
		 //            "type": "string",
		 //            "enum": [
		 //                "1/km",
		 //                "1/m",
		 //                "1/cm",
		 //                "1/mm",
		 //                "1/in",
		 //                "1/ft",
		 //                "1/mi"
		 //            ],
		 //            "defaultsTo": "1/m"
		 //        },
		 //        "project": {
		 //            "model": "project"
		 //        },
		 //        "objType":{ 
			//         "type": 'string',
			//         "enum": [ 'MFlowProjectMeasurementUnits' ],
			//         "defaultsTo": 'MFlowProjectMeasurementUnits' 
			//     },
		 //        "objFormat": {
			// 	    "type": 'string',
			// 	    "enum": [ 'json' ],
			// 	    "defaultsTo": 'json' 
			// 	},
		 //        "objSource": {
			// 	    "type": 'string',
			// 	    "enum": [ 'MFlowApp', 'UserImport' ],
			// 	    "defaultsTo": 'MFlowApp' 
			// 	},
			// 	"schemaVersion": { "type": 'string' },
		 //        "id": {
			//         "type": 'string',
			// 	    "autoIncrement": true,
			// 	    "primaryKey": true,
			// 	    "unique": true 
			// 	},
			//     "createdAt": { 
			//     	"type": 'datetime',
			//     	"default": 'NOW' 
			//     },
			//     "updatedAt": { 
			//     	"type": 'datetime', 
			//     	"default": 'NOW' 
			//     }
		 //    },
		 //    "datasets": {
		 //        "collection": "dataset",
		 //        "via": "project",
	  //           "defaultsTo": []
		 //    },
		 //    "models": {
		 //        "collection": "model",
		 //        "via": "project",
		 //        "defaultsTo": []
		 //    },
		 //    "owner": {
		 //        "type": "string",
		 //        "required": true
		 //    },
		 //    "objType": {
			// 	"type": 'string',
			// 	"enum": [ 'MFlowProject' ],
		 //        "defaultsTo": 'MFlowProject' 
		 //    },
			// "objFormat":{ 
			//     "type": 'string',
			//     "enum": [ 'json' ],
			//     "defaultsTo": 'json' 
			// },
			// "objSource": {
			//     "type": 'string',
			//     "enum": [ 'MFlowApp', 'UserImport' ],
			//     "defaultsTo": 'MFlowApp' 
			// },
		 //    "schemaVersion": {
		 //        "type": "string",
		 //        "defaultsTo": undefined 
		 //    },
		 //    "id":{ 
			// 	"type": 'string',
		 //        "autoIncrement": true,
		 //        "primaryKey": true,
		 //        "unique": true 
		 //    },
		 //    "createdAt": { 
		 //    	"type": 'datetime',
		 //    	"default": 'NOW' 
		 //    },
			// "updatedAt": {
			//     "type": 'datetime',
			//     "default": 'NOW' 
			// }	    
	  //   }
   //  })
.toss()


frisby.create('Login as admin should return success')
    .post('http://api.quarkcloudtech.com:3030/api/sessions',{
        "email":"admin@quarkcloudtech.com",
        "password":"zzxxcc12345"
    })
    .expectStatus(200)
    .expectHeaderContains('Content-Type','application/json')
    .inspectBody()
    .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object,
        result:{
            token:String
        }
    })
    .expectJSON({
        success:true,
        errorCode:0,
        errors:[],
        errfor:{},
    })
    .afterJSON(function(body){
     	access_token=body.result.token;
     	frisby.create('Post create a new project should return sucess')
	        .post('http://api.quarkcloudtech.com:1337/api/projects',{
	            "name" : "Test Project by Isabel",
	          	"desc" : "This is a test project created on 12.12",
	          	"tags" : [ "test", "isabel", "12.12"],
	         	"crs" : "WGS84",
	          	"measurementUnit" : {
		            "length" : "m",
		            "time" : "day",
		            "pumpingRate" : "m3/s",
		            "rechargeRate" : "cm/s",
		            "specificStorage" : "1/m"
	            }
	        },{
	         	headers:{
		           'Authorization':'bearer '+access_token,
		           'uid':[]
	            }
	        })
	        .expectStatus(200)
	        .expectHeaderContains('Content-Type','application/json')
	        .inspectJSON()
        .toss() 
    })
    .afterJSON(function(body){
        //access_token=body.result.token;
        frisby.create('Get list users projects should return sucess')
	        .get('http://api.quarkcloudtech.com:1337/api/projects',{
	            headers:{
	            	'Authorization':'bearer '+access_token,
	            	'uid':[]
	          	}
	        })
	        .expectStatus(200)
	        //.expectHeaderContains('Content-Type','application/json')
	        .inspectJSON()
        .toss() 
    })
    .afterJSON(function(body){
        //access_token=body.result.token;
        frisby.create('Get a project info should return sucess')
	        .get('http://api.quarkcloudtech.com:1337/api/projects/548e7d9bf8c661835d7f1c79',{
	            headers:{
	            	'Authorization':'bearer '+access_token,
	            	'uid':[]
	          	}
	        })
	        .expectStatus(200)
	        //.expectHeaderContains('Content-Type','application/json')
	        .inspectJSON()
	        .afterJSON(function(){
	        	frisby.create('Put update project should return sucess')
			        .get('http://api.quarkcloudtech.com:1337/api/projects/5473efbc5308759d571709d7',{
			            "name" : "Test Project by Isabel updated",
			          	"desc" : "This is a test project updated",
			          	"tags" : [ "test", "isabel", "12.12","updated"],
			         	"crs" : "WGS84",
			          	"measurementUnit" : {
				            "length" : "m",
				            "time" : "day",
				            "pumpingRate" : "m3/s",
				            "rechargeRate" : "cm/s",
				            "specificStorage" : "1/m"
			            }
	                },{
			            headers:{
			            	'Authorization':'bearer '+access_token,
			            	'uid':[]
			          	}
			        })
			        .expectStatus(200)
			        //.expectHeaderContains('Content-Type','application/json')
			        .inspectJSON()
			        .afterJSON(function(){
			        	frisby.create('Get a project info after updated should return sucess')
					        .get('http://api.quarkcloudtech.com:1337/api/projects/5473efbc5308759d571709d7',{
					            headers:{
					            	'Authorization':'bearer '+access_token,
					            	'uid':[]
					          	}
					        })
					        .expectStatus(200)
					        //.expectHeaderContains('Content-Type','application/json')
					        .inspectJSON()
					    .toss()
					    .afterJSON(function(){
				        	frisby.create('DELETE a project should return sucess')
						        .delete('http://api.quarkcloudtech.com:1337/api/545336e4d86c76191183a17c/projects/5473efbc5308759d571709d7',{
						            headers:{
						            	'Authorization':'bearer '+access_token,
						            	'uid':[]
						          	}
						        })
						        .expectStatus(200)
						        //.expectHeaderContains('Content-Type','application/json')
						        .inspectJSON()
						    .toss()
				        })
			        })
		        .toss()
	        })
        .toss() 
    })

.toss()