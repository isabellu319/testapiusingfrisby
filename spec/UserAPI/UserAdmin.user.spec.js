 
/* frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

//Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});     
 
/*
 *  Login as user to test  
 */ 

var access_token1="";

frisby.create('Successfully login as user should return success')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',{
  		"email":"shiji.lu319@gmail.com",
  		"password":"12345"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
  	success:Boolean,
  	errorCode:Number,
  	errors:Array,
  	errfor:Object,
  	result:{
  		token:String
  	}
  })
  .expectJSON({
  	success:true,
  	errorCode:0,
  	errors:[],
  	errfor:{},
  })
  .afterJSON(function(body){
  	access_token1=body.result.token;
  	// console.log('This is the access token got: '+access_token1);

  	frisby.create('Get List of Users should return false because user does not have admin privilege')
  	.get('http://api.quarkcloudtech.com:3030/api/users',{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    // .expectJSONTypes({
    //   success:Boolean,
    //   errorCode:Number,
    //   errors:Array,
    //   errfor:Object,
    //   result:{
    //     user:Object
    //   }
    // })
    // .expectJSON({
    //   success:false,
    //   errorCode:403,
    //   errors:['User doesn\'t have admin privilege.'],
    //   errfor:{},
    // })
  	.inspectJSON()
  	.toss();
  })
  .afterJSON(function(body){

  	frisby.create('Get User Info by ID should return false because user does not have admin privilege')
  	.get('http://api.quarkcloudtech.com:3030/api/users/545336e4d86c76191183a17c/',{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    // .expectJSONTypes({
    //   success:Boolean,
    //   errorCode:Number,
    //   errors:Array,
    //   errfor:Object,
    //   result:{
    //     user:Object
    //   }
    // })
    // .expectJSON({
    //   success:false,
    //   errorCode:403,
    //   errors:['User doesn\'t have admin privilege.'],
    //   errfor:{},
    // })
  	.inspectJSON()
  	.toss();
  })
  .afterJSON(function(body){

  	frisby.create('Put update User Info by ID should return false because user does not have admin privilege')
  	.put('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  		"isActive":"yes",
  		"role":"user",
  		"fullName":"Isabel Lulu"
  	},{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    // .expectJSONTypes({
    //   success:Boolean,
    //   errorCode:Number,
    //   errors:Array,
    //   errfor:Object,
    //   result:{
    //     user:Object
    //   }
    // })
    // .expectJSON({
    //   success:false,
    //   errorCode:403,
    //   errors:['User doesn\'t have admin privilege.'],
    //   errfor:{},
    // })
  	.inspectJSON()
  	.toss();
  })
  .afterJSON(function(body){
  	frisby.create('Put Change user password should return false because user does not have admin privilege')
  	.put('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/password',{
  		"password":"changed",
  		"confirmation":"changed"
  	},{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    // .expectJSONTypes({
    //   success:Boolean,
    //   errorCode:Number,
    //   errors:Array,
    //   errfor:Object,
    //   result:{
    //     user:Object
    //   }
    // })
    // .expectJSON({
    //   success:false,
    //   errorCode:403,
    //   errors:['User doesn\'t have admin privilege.'],
    //   errfor:{},
    // })
  	.inspectJSON()
  	.toss();
  })
  .afterJSON(function(body){
  	frisby.create('Delete a user by id should return false because user does not have admin privilege')
  	.delete('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    // .expectJSONTypes({
    //   success:Boolean,
    //   errorCode:Number,
    //   errors:Array,
    //   errfor:Object,
    //   result:{
    //     user:Object
    //   }
    // })
    // .expectJSON({
    //   success:false,
    //   errorCode:403,
    //   errors:[ ], // This is empty,should have a
    //   errfor:{},
    // })
  	.inspectJSON()          
  	.toss();
  })

.toss();

/*
 *  end of Login as admin to test  
 */








