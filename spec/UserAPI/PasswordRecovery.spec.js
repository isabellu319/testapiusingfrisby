
/* frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

//Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});   
 
/*
 *  Forgot Password Request
 */ 

frisby.create('Post Forgot Password Request should return success')
  .post('http://api.quarkcloudtech.com:3030/api/users/forgot/',{
  	"email":"shiji.lu319@gmail.com"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':true,
    'errorCode':0,
    'errors':[],
    'errfor':{}
  })
.toss();

frisby.create('Post Forgot Password Request should return false because email required')
  .post('http://api.quarkcloudtech.com:3030/api/users/forgot/',{
    "email":""
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':400,
    'errors':[],
    'errfor':{email: 'required'}
  })
.toss();

frisby.create('Post Forgot Password Request should return false because of invalid email format')
  .post('http://api.quarkcloudtech.com:3030/api/users/forgot/',{
    "email":"www.baidu.com"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':403,
    'errors':[],
    'errfor':{}
  })
.toss(); // Got User not found when email address format is invalid

frisby.create('Post Forgot Password Request should return false because email not registered')
  .post('http://api.quarkcloudtech.com:3030/api/users/forgot/',{
    "email":"shiji.lu@gmail.com"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':403,
    'errors':['User not found.'],
    'errfor':{}
  })
.toss();

/*
 *  end of Forgot Password Request
 */






