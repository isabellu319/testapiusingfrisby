/*frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

 var frisby = require('/usr/local/lib/node_modules/frisby');

 //Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});  

/*
 *  Check Password Reset Token
 */ 

frisby.create('Get Check Password Reset Token should return false because invalid token')
  .get('http://api.quarkcloudtech.com:3030/api/users/reset/shiji.lu319@gmail.com/e9cdd66d82126d24e2da2309f4c892aca74436bec5/')
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':403,
    'errors':['Invalid or expired token.'],
    'errfor':{}
  })
.toss();

frisby.create('Get Check Password Reset Token should return success')
  .get('http://api.quarkcloudtech.com:3030/api/users/reset/shiji.lu319@gmail.com/2e01dd6464fb71f02c812ae61b8d6263b1ac3aed1b/')
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':true,
    'errorCode':0,
    'errors':[],
    'errfor':{}
  })
.toss();

/*
 *  end of Check Password Reset Token
 */

/*
 *  Reset Password 
 */ 

frisby.create('Put Password Reset should return success')
  .put('http://api.quarkcloudtech.com:3030/api/users/reset/shiji.lu319@gmail.com/2e01dd6464fb71f02c812ae61b8d6263b1ac3aed1b/',{
  	"password":"12345",
  	"confirmation":"12345"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':true,
    'errorCode':0,
    'errors':[],
    'errfor':{}
  })
.toss();


frisby.create('Put  Password Reset should return false because passwords do not match')
  .put('http://api.quarkcloudtech.com:3030/api/users/reset/shiji.lu319@gmail.com/2e01dd6464fb71f02c812ae61b8d6263b1ac3aed1b/',{
  	"password":"isabel1",
  	"confirmation":"isabel"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':400,
    'errors':[],
    'errfor':{confirmation: 'Passwords do not match.'}
  })
.toss();

/*
 *  end of Reset Password 
 */