/**
 * frisby.js: quarkcloud API Testing
 * 7/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

//Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});   
   

var access_token="";

//Test log in using unregistered email
frisby.create('Post unregistered user should return "Email is not registered."')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',{
  	  "email":"shiji.lu@gmail.com",
  		"password":"isabel"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
  	success:Boolean,
  	errorCode:Number,
  	errors:Array,
  	errfor:Object
  })
  .expectJSON({
  	'success':false,
  	'errorCode':404,
  	'errors':['Email is not registered.'],
  	'errfor':{}
  })
.toss() 


//test log in using wrong username password combination
frisby.create('Post wrong username password combination should return "Invalid password."')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',
  {
    "email":"admin@quarkcloudtech.com",
    "password":"zzxxcc1234" //correct should be zzxxcc12345
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':401,
    'errors':['Invalid password.'],
    'errfor':{}
  })
.toss()


//Test log in using inactive account
frisby.create('Post inactive login should return "Account is inactive"')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',{
      "email":"shiji.lu319@gmail.com",
      "password":"isabel"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':403,
    'errors':['The user account is disabled.'],
    'errfor':{}
  })
.toss()

//test log in with no email or password 
frisby.create('Post wrong username password combination should return email password required')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',
  {
    "email":"",
    "password":"" //correct should be zzxxcc12345
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':400,
    'errors':[],
    'errfor':{ email: 'required', password: 'required'}
  })
.toss()

//test log in should return success
frisby.create('Post log in should return success')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',
  {
    "email":"admin@quarkcloudtech.com",
    "password":"zzxxcc12345" 
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':true,
    'errorCode':0,
    'errors':[],
    'errfor':{}
  })
  .afterJSON(function(body){
    access_token=body.result.token;
    frisby.create('Get Verify should return success')
      .get('http://api.quarkcloudtech.com:3030/api/sessions',{
        headers:{
          'Authorization':'bearer '+access_token
        }
      })
      .inspectJSON()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':true,
        'errorCode':0,
        'errors':[],
        'errfor':{}
      })
      .afterJSON(function(){
        
        frisby.create('Delete Log out should return false because invalid access token')
          .delete('http://api.quarkcloudtech.com:3030/api/sessions',{
            headers:{
              'Authorization':'bearer '+access_token+'make this invalid'
            }
          })
          .inspectJSON()
          .expectJSONTypes({
            success:Boolean,
            errorCode:Number,
            errors:Array,
            errfor:Object
          })
          .expectJSON({
            success:false,
            errorCode:403,
            errors:[],
            errfor:{},
          })
          .afterJSON(function(){
            console.log(access_token)
            frisby.create('Delete Log out should return success')
              .delete('http://api.quarkcloudtech.com:3030/api/sessions',{
                headers:{
                  'Authorization':'bearer '+access_token
                }
              })
              .inspectJSON()
              .expectJSONTypes({
                success:Boolean,
                errorCode:Number,
                errors:Array,
                errfor:Object
              })
              .expectJSON({
                success:true,
                errorCode:0,
                errors:[],
                errfor:{},
              }) // Can not delete log out successfully!!!!!
            .toss()
          })
        .toss();
      })
    .toss()
  })
.toss()










