/*
 * frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

 //Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
}); 
  

// success registration
frisby.create('Post correct information for registration should return success')
  .post('http://api.quarkcloudtech.com:3030/api/users',{
  		"email":"shiji.lu319@gmail.com",
  		"password":"isabel",
      "confirmation":"isabel" 
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectBody()
  .expectJSONTypes({
  	success:Boolean,
  	errorCode:Number,
  	errors:Array,
  	errfor:Object
  })
  .expectJSON({
  	'success':true,
  	'errorCode':0,
  	'errors':[],
  	'errfor':{}
  })
  .afterJSON(function(body){
    frisby.create('Post registration again should return false because email is already registered')
      .post('http://api.quarkcloudtech.com:3030/api/users',{
        "email":"shiji.lu319@gmail.com",
        "password":"isabel",
        "confirmation":"isabel" 
      })
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':false,
        'errorCode':400,
        'errors':[],
        'errfor':{"email":"Email is already registered."}
      })
    .toss()
  })
  .afterJSON(function(body){
    frisby.create('Post resend verification should return success')
      .get('http://api.quarkcloudtech.com:3030/api/users/verification/send/shiji.lu319@gmail.com')
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':true,
        'errorCode':0,
        'errors':[],
        'errfor':{}
      })
      .afterJSON(function(body){ 
        frisby.create('Get verify user should return false because email required')
          .get('http://api.quarkcloudtech.com:3030/api/users/verify//')
          .expectStatus(200)
          .expectHeaderContains('Content-Type','application/json')
          .inspectBody()
          .expectJSONTypes({
            success:Boolean,
            errorCode:Number,
            errors:Array,
            errfor:Object
          })
          .expectJSON({
            'success':false,
            'errorCode':400,
            'errors':[],
            'errfor':{"email":"required"}
          })
        .toss()
      })// got 404
      .afterJSON(function(body){
        frisby.create('Get verify user should return false because invalid email format')
          .get('http://api.quarkcloudtech.com:3030/api/users/verify/shiji.lu319//')
          .expectStatus(200)
          .expectHeaderContains('Content-Type','application/json')
          .inspectBody()
          .expectJSONTypes({
            success:Boolean,
            errorCode:Number,
            errors:Array,
            errfor:Object
          })
          .expectJSON({
            'success':true,
            'errorCode':0,
            'errors':[],
            'errfor':{"email":"Invalid email format"}
          })
        .toss()
      })// got 404
      .afterJSON(function(body){
        frisby.create('Get verify user should return false because email is not registerd')
          .get('http://api.quarkcloudtech.com:3030/api/users/verify/shiji.lu@gmail.com//')
          .expectStatus(200)
          .expectHeaderContains('Content-Type','application/json')
          .inspectBody()
          .expectJSONTypes({
            success:Boolean,
            errorCode:Number,
            errors:Array,
            errfor:Object
          })
          .expectJSON({
            'success':true,
            'errorCode':0,
            'errors':[],
            'errfor':{"email":"Email is not registered."}
          })
        .toss()
      })// got 404

    .toss()
  })
  .afterJSON(function(body){
    frisby.create('Post resend verification should return false because email required')
      .get('http://api.quarkcloudtech.com:3030/api/users/verification/send//')
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':false,
        'errorCode':400,
        'errors':[],
        'errfor':{"email":"required"}
      })
    .toss()
  })// got 404
  .afterJSON(function(body){
    frisby.create('Post resend verification should return false because invalid email format')
      .get('http://api.quarkcloudtech.com:3030/api/users/verification/send/imadethisup//')
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':false,
        'errorCode':400,
        'errors':[],
        'errfor':{"email":"Invalid email format"}
      })
    .toss()
  })// got 404
  .afterJSON(function(body){
    frisby.create('Post resend verification should return false because email is not registered')
      .get('http://api.quarkcloudtech.com:3030/api/users/verification/send/shiji.lu@gmail.com//')
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':false,
        'errorCode':400,
        'errors':[],
        'errfor':{"email":"Email is not registered."}
      })
    .toss()
  })// got 404
.toss();


// blank email and password
frisby.create('Post blank email and password should return email required & password required')
  .post('http://api.quarkcloudtech.com:3030/api/users',{
      "email":"",
      "password":"",
      "confirmation":"" 
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectBody()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':400,
    'errors':[],
    'errfor':{"email":"required","password":"required"}
  })
.toss();


// invalid email format & passwords do not match
frisby.create('Post invalid email address and unmatched passwords should return invalid email format & passwords do not match')
  .post('http://api.quarkcloudtech.com:3030/api/users',{
      "email":"imadethisup",
      "password":"123",
      "confirmation":"1234" 
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectBody()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':false,
    'errorCode':400,
    'errors':[],
    'errfor':{"email":"Invalid email format","confirmation":"Passwords do not match."}
  })
.toss();









 
 
 
 