/*
 * frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

 //Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
}); 

frisby.create('Get verify user should return success')
  .get('http://api.quarkcloudtech.com:3030/api/users/verify/shiji.lu319@gmail.com/f44220c80b3df771c5100be23ac436bf2bb27b52e6/')
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectBody()
  .expectJSONTypes({
    success:Boolean,
    errorCode:Number,
    errors:Array,
    errfor:Object
  })
  .expectJSON({
    'success':true,
    'errorCode':0,
    'errors':[],
    'errfor':{}
  })
  .afterJSON(function(body){
    frisby.create('Get resend verification should return false because user has already been verified')
      .get('http://api.quarkcloudtech.com:3030/api/users/verification/send/shiji.lu319@gmail.com/')
      .expectStatus(200)
      .expectHeaderContains('Content-Type','application/json')
      .inspectBody()
      .expectJSONTypes({
        success:Boolean,
        errorCode:Number,
        errors:Array,
        errfor:Object
      })
      .expectJSON({
        'success':false,
        'errorCode':400,
        'errors':[],
        'errfor':{"email":"User has already been verified"} //Issue: Email has blah blah
      })
    .toss()
  })
.toss()