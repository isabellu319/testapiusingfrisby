/* frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

 //Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
}); 
   
/*
 *  User Info
 */ 

var access_token="";

frisby.create('Successfully login should return success')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',{
  		// "email":"admin@quarkcloudtech.com",
  		// "password":"zzxxcc12345"
  		"email":"shiji.lu319@gmail.com",
  		"password":"12345"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectJSON()
  .expectJSONTypes({
  	success:Boolean,
  	errorCode:Number,
  	errors:Array,
  	errfor:Object,
  	result:{
  		token:String
  	}
  })
  .expectJSON({
  	success:true,
  	errorCode:0,
  	errors:[],
  	errfor:{},
  })
  .afterJSON(function(body){
  	access_token=body.result.token;
  	//console.log(access_token);

  	frisby.create('Get User Info should return success')
  	.get('http://api.quarkcloudtech.com:3030/api/users/me',{
  		headers:{
  			'Authorization':'bearer '+access_token
  		}
  	})
    .expectStatus(200)
    .expectHeaderContains('Content-Type','application/json')
  	.inspectJSON()
    .expectJSONTypes({
      success:Boolean,
      errorCode:Number,
      errors:Array,
      errfor:Object,
      result:{
        user:Object
      }
    })
    .expectJSON({
      success:true,
      errorCode:0,
      errors:[],
      errfor:{},
    })
  	.toss();
  })
  .afterJSON(function(body){
  	frisby.create('Put Update User Profile should return success')
  	.put('http://api.quarkcloudtech.com:3030/api/users/me',{
  		"fullName":"Isabel Lu updated during testing"  		
  	},{
  		headers:{
  			'Authorization':'bearer '+access_token
  		}
  	})
    .expectStatus(200)
    .expectHeaderContains('Content-Type','application/json')
  	.inspectJSON()
    .expectJSONTypes({
      success:Boolean,
      errorCode:Number,
      errors:Array,
      errfor:Object,
    })
    .expectJSON({
      success:true,
      errorCode:0,
      errors:[],
      errfor:{},
    })
  	.toss();
  })
  .afterJSON(function(body){
  	frisby.create('Get Updated User Profile should return false because fullName required')
  	.get('http://api.quarkcloudtech.com:3030/api/users/me',{
      'fullName':''
    },{
  		headers:{
  			'Authorization':'bearer '+access_token
  		}
  	})
  	.inspectJSON()
    .expectStatus(403) // This status code should be 200
    .expectHeaderContains('Content-Type','application/json')
    .expectJSONTypes({
      success:Boolean,
      errorCode:Number,
      errors:Array,
      errfor:Object,
    })
    .expectJSON({
      success:false,
      errorCode:403,
      errors:[],
      errfor:{},
    })
  	.toss();
  })
  .afterJSON(function(body){
    frisby.create('Get Updated User Profile should return false because fullName only use letters,spaces and numbers')
    .get('http://api.quarkcloudtech.com:3030/api/users/me',{
      'fullName':'Isabel@#$%^'
    },{
      headers:{
        'Authorization':'bearer '+access_token
      }
    })
    .expectStatus(403) // This status code should be 200
    .expectHeaderContains('Content-Type','application/json')
    .inspectJSON()
    .expectJSONTypes({
      success:Boolean,
      errorCode:Number,
      errors:Array,
      errfor:Object,
    })
    .expectJSON({
      success:false,
      errorCode:403,
      errors:[],
      errfor:{},
    })

    // Got 404 trying to change password!!!!!!!!!!
    .afterJSON(function(body){
      frisby.create('Get Change password should return success')
        .get('http://api.quarkcloudtech.com:3030/api/users/me/password',{
          "password":"123",
          "confirmation":"123"
        },{
          headers:{
          'Authorization':'bearer '+access_token
          }
        })
        .expectStatus(200)
        .inspectJSON()
        .expectJSONTypes({
          success:Boolean,
          errorCode:Number,
          errors:Array,
          errfor:Object,
        })
        .expectJSON({
          success:true,
          errorCode:0,
          errors:[],
          errfor:{},
        })
        .afterJSON(function(){
          frisby.create('Get change password should return false because password and confirmation required')
            .get('http://api.quarkcloudtech.com:3030/api/users/me/password',{
              "password":"",
              "confirmation":""
            },{
              headers:{
              'Authorization':'bearer '+access_token
              }
            })
            .expectStatus(200)
            .inspectJSON()
            .expectJSONTypes({
              success:Boolean,
              errorCode:Number,
              errors:Array,
              errfor:Object,
            })
            .expectJSON({
              success:false,
              errorCode:403,
              errors:[],
              errfor:{},
            })
          .toss()
        })
        .afterJSON(function(){
          frisby.create('Get change password should return false because passwords do not match')
            .get('http://api.quarkcloudtech.com:3030/api/users/me/password',{
              "password":"1",
              "confirmation":"2"
            },{
              headers:{
              'Authorization':'bearer '+access_token
              }
            })
            .expectStatus(200)
            .inspectJSON()
            .expectJSONTypes({
              success:Boolean,
              errorCode:Number,
              errors:Array,
              errfor:Object,
            })
            .expectJSON({
              success:false,
              errorCode:403,
              errors:[],
              errfor:{},
            })
          .toss()
        })
        .toss();
      })

    .toss();
  })  
.toss();

/*
 *  end of User Info
 */
