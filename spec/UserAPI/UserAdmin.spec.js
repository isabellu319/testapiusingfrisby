
/* frisby.js: quarkcloud API Testing
 * 12/11/2014 Isabel Lu
 */

var frisby = require('/usr/local/lib/node_modules/frisby');

//Global setup for all tests
frisby.globalSetup({
  request: {
    headers:{'Accept': 'application/json'}
  }
});     
    
/*
 *  Login as admin to test  
 */ 

var access_token1="";

frisby.create('Successfully login as admin should return success')
  .post('http://api.quarkcloudtech.com:3030/api/sessions',{
  		"email":"admin@quarkcloudtech.com",
  		"password":"zzxxcc12345"
  })
  .expectStatus(200)
  .expectHeaderContains('Content-Type','application/json')
  .inspectBody()
  .expectJSONTypes({
  	success:Boolean,
  	errorCode:Number,
  	errors:Array,
  	errfor:Object,
  	result:{
  		token:String
  	}
  })
  .expectJSON({
  	success:true,
  	errorCode:0,
  	errors:[],
  	errfor:{},
  })
  // .afterJSON(function(body){
  // 	access_token1=body.result.token;
  // 	// console.log('This is the access token got: '+access_token1);

  // 	frisby.create('Get List of Users should return success')
  // 	.get('http://api.quarkcloudtech.com:3030/api/users',{
  // 		headers:{
  // 			'Authorization':'bearer '+access_token1
  // 		}
  // 	})
  //   .expectJSONTypes({
  //     data:Object
  //   })
  // 	.inspectJSON()
  //   //***************Issues**************//
  //   /* The response does not have success errors blahblah */
  // 	.toss();
  // })
  // .afterJSON(function(body){

  // 	frisby.create('Get User Info by ID should return success')
  // 	.get('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  // 		headers:{
  // 			'Authorization':'bearer '+access_token1
  // 		}
  // 	})
  //   .expectJSONTypes({
  //     success:Boolean,
  //     errorCode:Number,
  //     errors:Array,
  //     errfor:Object,
  //     result:{
  //       user:Object
  //     }
  //   })
  //   .expectJSON({
  //     success:true,
  //     errorCode:0,
  //     errors:[],
  //     errfor:{},
  //   })
  // 	.inspectJSON()
  // 	.toss();
  // })
  // .afterJSON(function(body){

  // 	frisby.create('Put update User Info by ID should return success')
  // 	.put('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  // 		"isActive":"yes",
  // 		"role":"user",
  // 		"fullName":"Isabel Lulu"
  // 	},{
  // 		headers:{
  // 			'Authorization':'bearer '+access_token1
  // 		}
  // 	})
  //   .expectJSONTypes({
  //     success:Boolean,
  //     errorCode:Number,
  //     errors:Array,
  //     errfor:Object
  //   })
  //   .expectJSON({
  //     success:true,
  //     errorCode:0,
  //     errors:[],
  //     errfor:{},
  //   })
  // 	.inspectJSON()
  //   .afterJSON(function(){
  //     frisby.create('Put update User Info by ID should return error')
  //       .put('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  //         "isActive":"nope",
  //         "role":"admi",
  //         "fullName":""
  //       },{
  //         headers:{
  //           'Authorization':'bearer '+access_token1
  //         }
  //       })
  //       .expectJSONTypes({
  //         success:Boolean,
  //         errorCode:Number,
  //         errors:Array,
  //         errfor:Object
  //       })
  //       .expectJSON({
  //         success:false,
  //         errorCode:0, //this should not be 0
  //         errors:[],
  //         errfor:{
  //           isActive:'invalid value.',
  //           role:'invalid role',
  //           fullName:'required'
  //         },
  //       })
  //       .inspectJSON()
  //       //***************Issues**************//
  //       /* errorCode should not be 0 here since success is false */
  //       /*fullName in errfor is empty when try to pass "@#$%^" to fullName, which should return "only blahblah"*/
  //       .toss()
  //     })
  //     .afterJSON(function(){
  //       frisby.create('Put update User Info by ID should return error')
  //         .put('http://api.quarkcloudtech.com:3030/api/users/imadethisup/',{
  //           "isActive":"yes",
  //           "role":"user",
  //           "fullName":"#$%^&"
  //         },{
  //           headers:{
  //             'Authorization':'bearer '+access_token1
  //           }
  //         })
  //         .expectJSONTypes({
  //           success:Boolean,
  //           errorCode:Number,
  //           errors:Array,
  //           errfor:Object
  //         })
  //         .expectJSON({
  //           success:false,
  //           errorCode:500,
  //           errors:[],
  //           errfor:{}
  //         })
  //         .inspectJSON()
  //         //***************Issues**************//
          
  //         /*fullName in errfor is empty when try to pass "@#$%^" to fullName, which should return "only blahblah"*/
  //         /*invalid user ID should return errors "User is not found" errorCode is 500*/
  //         /*'Exception: CastError: Cast to ObjectId failed for value "imadethisup" at path "_id"' */
  //         .toss()
  //     })
  // 	.toss();
  // })
  // .afterJSON(function(body){

  // 	frisby.create('Get Updated User Info by ID should return success')
  // 	.get('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/',{
  // 		headers:{
  // 			'Authorization':'bearer '+access_token1
  // 		}
  // 	})
  //   .expectJSONTypes({
  //     success:Boolean,
  //     errorCode:Number,
  //     errors:Array,
  //     errfor:Object,
  //     result:{
  //       user:Object
  //     }
  //   })
  //   .expectJSON({
  //     success:true,
  //     errorCode:0,
  //     errors:[],
  //     errfor:{},
  //   })
  // 	.inspectJSON()
  // 	.toss();
  // })
  // .afterJSON(function(body){
  // 	frisby.create('Put Change user password should return success')
  // 	.put('http://api.quarkcloudtech.com:3030/api/users/546af0d1ed357c757dc9d133/password',{
  // 		"password":"changed",
  // 		"confirmation":"changed"
  // 	},{
  // 		headers:{
  // 			'Authorization':'bearer '+access_token1
  // 		}
  // 	})
  //   .expectJSONTypes({
  //     success:Boolean,
  //     errorCode:Number,
  //     errors:Array,
  //     errfor:Object
  //   })
  //   .expectJSON({
  //     success:true,
  //     errorCode:0,
  //     errors:[],
  //     errfor:{},
  //   })
  // 	.inspectJSON()
  // 	.toss();
  // })
  .afterJSON(function(body){
  	frisby.create('Delete a user by id should return success')
  	.delete('http://api.quarkcloudtech.com:3030/api/users/548b9fcf3710c10263262345/',{
  		headers:{
  			'Authorization':'bearer '+access_token1
  		}
  	})
    .expectJSONTypes({
      success:Boolean,
      errorCode:Number,
      errors:Array,
      errfor:Object
    })
    .expectJSON({
      success:false,
      errorCode:403, //can not delete user
      errors:[],
      errfor:{}
    })
  	.inspectJSON()
    .afterJSON(function(body){
      frisby.create('Get List of Users after deleting a user should return success')
      .get('http://api.quarkcloudtech.com:3030/api/users',{
        headers:{
          'Authorization':'bearer '+access_token1
        }
      })
      .expectJSONTypes({
        data:Object
      })
      .inspectJSON() 
      .toss();
    })
    //***************Issues**************//
    /* Can not delete User*/      
          
  	.toss();
  })

.toss();

/*
 *  end of Login as admin to test  
 */








